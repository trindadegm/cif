use image::GenericImageView;
use std::io::{Read, Write};

fn cti_encode<W: Write>(
    image: image::DynamicImage,
    pixel_format: cif::PixelFormat,
    compression: cif::CompressionLevel,
    writer: W,
) {
    let dimensions = image.dimensions();
    let pixels = match pixel_format {
        cif::PixelFormat::Rgb => image.to_rgb8().into_vec(),
        cif::PixelFormat::Rgba => image.to_rgba8().into_vec(),
    };
    cif::ImageBuilder::new()
        .with_dimensions(dimensions)
        .with_compression(compression)
        .with_pixel_format(pixel_format)
        .save_data(pixels.as_slice(), writer)
        .expect("failed to save image");
}

fn cti_decode<R: Read>(reader: R) -> image::DynamicImage {
    let reader = cif::ImageReader::new(reader).expect("failed to read image while creating reader");
    let (width, height) = reader.dimensions();
    let pixel_format = reader.pixel_format();
    let _compression = reader.compression();
    let mut buf = vec![0u8; reader.pixel_data_len().expect("integer error")];
    reader
        .read_pixels(buf.as_mut_slice())
        .expect("failed to read image data");

    match pixel_format {
        cif::PixelFormat::Rgb => image::DynamicImage::ImageRgb8(
            image::RgbImage::from_raw(width, height, buf)
                .expect("could not make image into RgbImage"),
        ),
        cif::PixelFormat::Rgba => image::DynamicImage::ImageRgba8(
            image::RgbaImage::from_raw(width, height, buf)
                .expect("could not make image into RgbaImage"),
        ),
    }
}

const IMAGES: [&'static str; 3] = [
    "tests/test_assets/cc0_png_example_600x600_0.png",
    "tests/test_assets/cc0_png_example_640x320_1.png",
    "tests/test_assets/cc0_png_example_395x600_2.png",
];

const PIXEL_FORMATS: [cif::PixelFormat; 2] = [cif::PixelFormat::Rgb, cif::PixelFormat::Rgba];

const COMPRESSION_LEVELS: [cif::CompressionLevel; 4] = [
    cif::CompressionLevel::None,
    cif::CompressionLevel::Fast,
    cif::CompressionLevel::Medium,
    cif::CompressionLevel::Best,
];

#[test]
pub fn test_losslessnes() {
    for img_path in IMAGES {
        for pixel_format in PIXEL_FORMATS {
            for compression_level in COMPRESSION_LEVELS {
                let img = image::open(img_path).unwrap();

                let mut outbuf = Vec::new();

                cti_encode(img.clone(), pixel_format, compression_level, &mut outbuf);

                let result = cti_decode(outbuf.as_slice());

                let (left, right) = match pixel_format {
                    cif::PixelFormat::Rgb => (
                        image::DynamicImage::ImageRgb8(img.to_rgb8()),
                        image::DynamicImage::ImageRgb8(result.to_rgb8()),
                    ),
                    cif::PixelFormat::Rgba => (
                        image::DynamicImage::ImageRgba8(img.to_rgba8()),
                        image::DynamicImage::ImageRgba8(result.to_rgba8()),
                    ),
                };
                assert_eq!(left, right);
            }
        }
    }
}
