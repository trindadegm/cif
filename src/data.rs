use std::{
    fmt::{self, Display, Formatter},
    io::{self, Read, Write},
    str::FromStr,
};

#[repr(transparent)]
pub struct RgbaPixel([u8; 4]);

#[repr(transparent)]
pub struct RgbPixel([u8; 3]);

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum PixelFormat {
    Rgb,
    Rgba,
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum Compression {
    No,
    Yes { compressed_data_len: usize },
}

#[derive(Clone, Copy, PartialEq, Eq, Debug)]
pub enum CompressionLevel {
    None,
    Fast,
    Medium,
    Best,
}

pub struct ImageReader<R: Read> {
    reader: R,
    dimensions: Dimensions,
    pixel_format: PixelFormat,
    compression: Compression,
}

pub type Dimensions = (u32, u32);

const HEADER_MAGIC_NUMBER: u32 = 0xB07EC081;
const FLAG_ALPHA_CHANNEL: u8 = 0x1;
const FLAG_COMPRESSION: u8 = 0x2;

impl<R: Read> ImageReader<R> {
    /// Creates a new image reader from any `Read`er.
    /// # Example
    /// ```rust
    /// # use std::fs::File;
    /// # use std::io::BufReader;
    /// # use cif::*;
    ///
    /// # fn check_if_compiles() -> Result<(), ImageError> {
    /// let mut imreader = ImageReader::new(
    ///     BufReader::new(File::open("your_image.cti")?)
    /// )?;
    /// # Ok(())
    /// # }
    /// ```
    pub fn new(mut reader: R) -> Result<Self, ImageError> {
        let mut magic_num_data = [0u8; 4];

        reader.read_exact(&mut magic_num_data)?;

        let magic_num = u32::from_be_bytes(magic_num_data);

        if magic_num != HEADER_MAGIC_NUMBER {
            return Err(ImageError::InvalidData);
        }

        let mut width_data = [0u8; 4];
        let mut height_data = [0u8; 4];

        reader.read_exact(&mut width_data)?;
        reader.read_exact(&mut height_data)?;

        let width = u32::from_be_bytes(width_data);
        let height = u32::from_be_bytes(height_data);

        let mut flags_data = [0u8; 4];

        reader.read_exact(&mut flags_data)?;

        let flags = flags_data[3];

        let pixel_format = if flags & FLAG_ALPHA_CHANNEL != 0 {
            PixelFormat::Rgba
        } else {
            PixelFormat::Rgb
        };

        let compression = if flags & FLAG_COMPRESSION != 0 {
            let mut compressed_data_len_data = [0u8; 8];
            reader.read_exact(&mut compressed_data_len_data)?;

            let compressed_data_len = u64::from_be_bytes(compressed_data_len_data);
            let compressed_data_len: usize = compressed_data_len
                .try_into()
                .map_err(|_| ImageError::OutOfMem)?;

            Compression::Yes {
                compressed_data_len,
            }
        } else {
            Compression::No
        };

        Ok(Self {
            reader,
            dimensions: (width, height),
            pixel_format,
            compression,
        })
    }

    /// Get the image dimentions (width, height) in pixels.
    /// # Example
    /// ```rust
    /// # use cif::ImageReader;
    /// # use std::io::Read;
    /// # fn check_if_compiles<R: Read>(mut cti_image_reader: ImageReader<R>) {
    /// let (width, height) = cti_image_reader.dimensions();
    /// # }
    /// ```
    #[inline]
    pub fn dimensions(&self) -> Dimensions {
        self.dimensions
    }

    /// Gets the pixel format of the image
    #[inline]
    pub fn pixel_format(&self) -> PixelFormat {
        self.pixel_format
    }

    /// Gets the compression information of the image
    #[inline]
    pub fn compression(&self) -> Compression {
        self.compression
    }

    /// Queries the number of pixels of the image as an `usize`
    /// # Result
    /// - `Ok(number_of_pixels)` when the number of pixels fits in a usize.
    /// - `Err(int_error)` when the number of pixels does not fit in a usize.
    pub fn num_pixels(&self) -> Result<usize, ImageError> {
        let (width, height) = self.dimensions;
        let width = u64::from(width);
        let height = u64::from(height);
        let num_pixels = width * height;
        usize::try_from(num_pixels)
            .map_err(|_| ImageError::IntError(std::num::IntErrorKind::PosOverflow))
    }

    /// Queries the byte length of the image as an `usize`
    /// # Result
    /// - `Ok(image_byte_length)` when byte length fits in a usize.
    /// - `Err(int_error)` when the byte length does not fit in a usize.
    pub fn pixel_data_len(&self) -> Result<usize, ImageError> {
        let num_pixels = self.num_pixels()?;
        num_pixels
            .checked_mul(self.pixel_format().length())
            .ok_or(ImageError::IntError(std::num::IntErrorKind::PosOverflow))
    }

    /// Reads the pixel data from the image
    ///
    /// # Example
    /// ```rust
    /// # use cif::*;
    /// # use std::fs::File;
    /// # use std::io::BufReader;
    /// # fn check_if_compiles() -> Result<(), ImageError> {
    /// let mut imreader = ImageReader::new(
    ///     BufReader::new(File::open("your_image.cti")?)
    /// )?;
    ///
    /// let data_len = imreader.pixel_data_len()?;
    ///
    /// let mut image_buffer = vec![0u8; data_len];
    /// imreader.read_pixels(image_buffer.as_mut_slice())?;
    /// # Ok(())
    /// # }
    /// ```
    /// # Notes
    /// - This function call consumes the `ImageReader`.
    /// - This function may allocate a vector for the possibly compressed data.
    ///   If you want to avoid such allocation, use `read_pixels_with_buffers`
    ///   instead. This function will not allocate an extra buffer if there is
    ///   no compression. You can check that with the method `compression`, that
    ///   returns information about the compression of the image.
    pub fn read_pixels(self, buf: &mut [u8]) -> Result<(), ImageError> {
        let mut compression_buf;
        let compression_buf = match self.compression {
            Compression::Yes {
                compressed_data_len,
            } => {
                compression_buf = vec![0u8; compressed_data_len];
                Some(compression_buf.as_mut_slice())
            }
            Compression::No => None,
        };
        self.read_pixels_with_buffers(buf, compression_buf)
    }

    /// Reads the pixel data from the image. If the image has compression, it
    /// will require an extra buffer parameter (otherwise it can be `None`).
    ///
    /// # Example
    /// ```rust
    /// # use cif::*;
    /// # use std::fs::File;
    /// # use std::io::BufReader;
    /// # fn check_if_compiles() -> Result<(), ImageError> {
    /// let mut imreader = ImageReader::new(
    ///     BufReader::new(File::open("your_image.cti")?)
    /// )?;
    ///
    /// let data_len = imreader.pixel_data_len()?;
    /// let mut image_buffer = vec![0u8; data_len];
    ///
    /// let mut compressed_data_vec = match imreader.compression() {
    ///     Compression::No => None,
    ///     Compression::Yes { compressed_data_len } => Some(vec![0u8; compressed_data_len]),
    /// };
    /// let compressed_data_buf: Option<&mut [u8]> = compressed_data_vec
    ///     .as_mut()
    ///     .map(|v| v.as_mut_slice());
    ///
    /// imreader.read_pixels_with_buffers(image_buffer.as_mut_slice(), compressed_data_buf)?;
    /// # Ok(())
    /// # }
    /// ```
    /// # Notes
    /// - This function call consumes the `ImageReader`.
    /// - This function will fail in case the image is compressed and
    ///   `compressed_data_buf` is `None`.
    pub fn read_pixels_with_buffers(
        mut self,
        output_image_buf: &mut [u8],
        compressed_data_buf: Option<&mut [u8]>,
    ) -> Result<(), ImageError> {
        let image_size = u64::from(self.dimensions.0) * u64::from(self.dimensions.1);
        let image_size = usize::try_from(image_size)
            .map_err(|_| ImageError::OutOfMem)?
            .checked_mul(self.pixel_format().length())
            .ok_or(ImageError::IntError(std::num::IntErrorKind::PosOverflow))?;

        let out_slice = output_image_buf
            .get_mut(0..image_size)
            .ok_or(ImageError::InvalidParams)?;

        match self.compression {
            Compression::Yes {
                compressed_data_len,
            } => {
                let compressed_aux_slice = compressed_data_buf
                    .and_then(|buf| buf.get_mut(0..compressed_data_len))
                    .ok_or(ImageError::InvalidParams)?;
                self.reader.read_exact(compressed_aux_slice)?;
                let mut decompressor = flate2::Decompress::new(false);
                decompressor.decompress(
                    &compressed_aux_slice,
                    out_slice,
                    flate2::FlushDecompress::Finish,
                )?;
            }
            Compression::No => {
                self.reader.read_exact(out_slice)?;
            }
        }
        Ok(())
    }
} // End of impl ImageReader

pub struct ImageBuilder {
    dimensions: Dimensions,
    pixel_format: PixelFormat,
    compression: CompressionLevel,
}

impl ImageBuilder {
    #[inline]
    pub fn new() -> ImageBuilder {
        Self::default()
    }

    /// The image dimension, in pixels (width, height).
    #[inline]
    pub fn with_dimensions(&mut self, dimensions: Dimensions) -> &mut Self {
        self.dimensions = dimensions;
        self
    }

    #[inline]
    pub fn with_pixel_format(&mut self, format: PixelFormat) -> &mut Self {
        self.pixel_format = format;
        self
    }

    #[inline]
    pub fn with_compression(&mut self, compression: CompressionLevel) -> &mut Self {
        self.compression = compression;
        self
    }

    /// Saves a `Image` in a file or any other `Write`r.
    ///
    /// # Example
    /// ```rust
    /// # use cif::*;
    /// # use std::fs::File;
    /// # use std::io::{Write, BufWriter};
    /// # fn check_if_compiles((width, height): Dimensions, pixel_data: &[u8]) -> Result<(), ImageError> {
    /// let mut file = BufWriter::new(File::create("my_image.cti")?);
    ///
    /// ImageBuilder::new()
    ///     .with_dimensions((width, height))
    ///     .with_pixel_format(PixelFormat::Rgba)
    ///     .with_compression(CompressionLevel::Fast)
    ///     .save_data(pixel_data, file)?;
    /// # Ok(())
    /// # }
    /// ```
    ///
    /// # Notes
    /// - Make sure the pixel_data is in the correct pixel format, otherwise
    ///   this function will fail.
    /// - Saving RGBA data in an image with RGB pixel format will cause the
    ///   saved image to be a corrupted mess.
    /// - By default, `ImageBuilder` sets the pixel format to RGBA,
    ///   compression is disabled, and the dimensions are zeroed.
    /// - If at least one of the image dimensions are zeroed, this function will
    ///   fail.
    pub fn save_data<W: Write>(
        &self,
        pixel_data: &[u8],
        mut writer: W,
    ) -> Result<(), ImageError> {
        if self.dimensions.0 == 0 || self.dimensions.1 == 0 {
            return Err(ImageError::InvalidParams);
        }

        let (width, height) = self.dimensions;
        let num_pixels = u64::from(width) * u64::from(height);
        let data_len = usize::try_from(num_pixels)
            .map_err(|_| ImageError::IntError(std::num::IntErrorKind::PosOverflow))
            .and_then(|n| {
                n.checked_mul(self.pixel_format.length())
                    .ok_or(ImageError::IntError(std::num::IntErrorKind::PosOverflow))
            })?;

        if pixel_data.len() != data_len {
            return Err(ImageError::InvalidParams);
        }

        writer.write_all(&HEADER_MAGIC_NUMBER.to_be_bytes())?;
        writer.write_all(&width.to_be_bytes())?;
        writer.write_all(&height.to_be_bytes())?;

        let compression = match self.compression {
            CompressionLevel::None => None,
            CompressionLevel::Fast => Some(flate2::Compression::fast()),
            CompressionLevel::Medium => Some(flate2::Compression::default()),
            CompressionLevel::Best => Some(flate2::Compression::best()),
        };

        let mut flags = 0u8;
        if compression.is_some() {
            flags |= FLAG_COMPRESSION;
        }
        if self.pixel_format == PixelFormat::Rgba {
            flags |= FLAG_ALPHA_CHANNEL;
        }
        writer.write_all(&[0, 0, 0, flags])?;

        if let Some(compression) = compression {
            let output_vec = Vec::with_capacity(pixel_data.len());

            let mut compressor = flate2::write::DeflateEncoder::new(output_vec, compression);
            compressor.write_all(pixel_data)?;

            let compressed_data = compressor.finish()?;
            let total_out: u64 = compressed_data
                .len()
                .try_into()
                .map_err(|_| ImageError::IntError(std::num::IntErrorKind::PosOverflow))?;
            writer.write_all(&total_out.to_be_bytes())?;

            writer.write_all(compressed_data.as_slice())?;
        } else {
            writer.write_all(pixel_data)?;
        }
        Ok(())
    }
}
impl Default for ImageBuilder {
    fn default() -> Self {
        Self {
            dimensions: (0, 0),
            pixel_format: PixelFormat::Rgba,
            compression: CompressionLevel::None,
        }
    }
}

impl PixelFormat {
    /// Returns the length of a pixel in this format in bytes.
    pub fn length(self) -> usize {
        match self {
            PixelFormat::Rgb => 3,
            PixelFormat::Rgba => 4,
        }
    }
}

#[derive(Debug)]
pub enum ImageError {
    DecompressFailed(flate2::DecompressError),
    CompressFailed(flate2::CompressError),
    IntError(std::num::IntErrorKind),
    InvalidData,
    InvalidParams,
    IoError(io::Error),
    OutOfMem,
}

impl std::error::Error for ImageError {}
impl Display for ImageError {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        use ImageError::*;
        match self {
            DecompressFailed(e) => write!(f, "Failed to decompress image: {}", e),
            CompressFailed(e) => write!(f, "Failed to compress image: {}", e),
            IntError(kind) => write!(f, "Integer error: {:?}", kind),
            InvalidData => write!(f, "Cannot decode: invalid data"),
            InvalidParams => write!(f, "Given parameters are not valid"),
            IoError(e) => write!(f, "Input/Output error: {}", e),
            OutOfMem => write!(f, "Program ran out of memory"),
        }
    }
}
impl From<io::Error> for ImageError {
    fn from(e: io::Error) -> Self {
        ImageError::IoError(e)
    }
}
impl From<flate2::DecompressError> for ImageError {
    fn from(e: flate2::DecompressError) -> Self {
        ImageError::DecompressFailed(e)
    }
}
impl From<flate2::CompressError> for ImageError {
    fn from(e: flate2::CompressError) -> Self {
        ImageError::CompressFailed(e)
    }
}

impl FromStr for CompressionLevel {
    type Err = ImageError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "none" => Ok(CompressionLevel::None),
            "fast" => Ok(CompressionLevel::Fast),
            "medium" => Ok(CompressionLevel::Medium),
            "best" => Ok(CompressionLevel::Best),
            _ => Err(ImageError::InvalidParams),
        }
    }
}
