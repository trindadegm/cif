use std::{io::BufWriter, fs::File};

use image::{GenericImageView, DynamicImage};

const HELP_MESSAGE: &'static str = r#"cifcvt 1.0.0
By Gustavo Moitinho Trindade
Takes a PNG or JPG image and converts to a CIF image
Usage:
    cifcvt [ OPTIONS ] <INPUT_FILENAME> <OUTPUT_FILENAME>
Options:
    -c | --compress <compression_level>
        Defines a compression level between the following:
            - none
            - fast
            - medium
            - best
        Defaults to none
    -f | --pixel-format <pixel_format>[
        Defines a pixel format between the following:
            - rgb
            - rgba
        Defaults to rgba

Image convertion may be lossy.
"#;

fn main() {
    let mut src_path = String::new();
    let mut dst_path = String::new();
    let mut compression = cif::CompressionLevel::None;
    let mut pixel_format = cif::PixelFormat::Rgba;

    enum ParseState {
        NextPath,
        Option(String),
    }
    let mut parse_state = ParseState::NextPath;
    for arg in std::env::args().skip(1) {
        if arg.starts_with("-") {
            parse_state = ParseState::Option(arg.clone());
            continue;
        }

        match parse_state {
            ParseState::Option(ref config) => match config.as_str() {
                "--compression" | "-c" => {
                    match arg.as_str() {
                        "none" => compression = cif::CompressionLevel::None,
                        "fast" => compression = cif::CompressionLevel::Fast,
                        "medium" => compression = cif::CompressionLevel::Medium,
                        "best" => compression = cif::CompressionLevel::Best,
                        _ => {
                            eprintln!("Unknown compression level {arg}");
                            std::process::exit(1);
                        }
                    }
                    parse_state = ParseState::NextPath;
                }
                "--pixel-format" | "-f" => {
                    match arg.as_str() {
                        "rgb" => pixel_format = cif::PixelFormat::Rgb,
                        "rgba" => pixel_format = cif::PixelFormat::Rgba,
                        _ => {
                            eprintln!("Unknown pixel format {arg}");
                            std::process::exit(1);
                        }
                    }
                    parse_state = ParseState::NextPath;
                }
                "--help" | "-h" => {
                    println!("{HELP_MESSAGE}");
                    std::process::exit(0);
                }
                _ => {
                    eprintln!("Unknown option {config}");
                    std::process::exit(1);
                }
            },
            ParseState::NextPath => {
                if src_path.is_empty() {
                    src_path = arg;
                } else {
                    dst_path = arg;
                }
            }
        }
    }
    if let ParseState::Option(optname) = parse_state {
        eprintln!("Unfinished option: {optname}");
        std::process::exit(1);
    }

    if src_path.is_empty() {
        eprintln!("Invalid usage: missing arguments. Printing help message:");
        eprintln!("{HELP_MESSAGE}");
        std::process::exit(1);
    }

    if dst_path.is_empty() {
        dst_path = src_path.clone();
        dst_path.push_str(".cif");
    }

    let out_file = BufWriter::new(File::create(dst_path).unwrap_or_else(|e| {
        eprintln!("Failed to create destination file: {e}");
        std::process::exit(1);
    }));

    let image = image::open(src_path).unwrap_or_else(|e| {
        eprintln!("Failed to open image: {e}");
        std::process::exit(1);
    });
    let image = match pixel_format {
        cif::PixelFormat::Rgb => {
            DynamicImage::from(image.into_rgb8())
        }
        cif::PixelFormat::Rgba => {
            DynamicImage::from(image.into_rgba8())
        }
    };

    cif::ImageBuilder::new()
        .with_compression(compression)
        .with_dimensions(image.dimensions())
        .with_pixel_format(pixel_format)
        .save_data(image.as_bytes(), out_file)
        .unwrap_or_else(|e| {
            eprintln!("Failed create cif image: {e}");
            std::process::exit(1);
        });
}
